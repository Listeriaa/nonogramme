let playedPicross = null;


class Timer {
    _intervalId;
    _currentTime;

    constructor(container) {
       
        this._container = container;
      }


    _resetTime() {
        this._currentTime = 0;

    }
    
    _updateDisplay() {
        this._container.innerHTML = this._currentTime + "s";
    }

    start() {
        this._resetTime();
        this._intervalId = setInterval(() => {
            this._currentTime ++;
            this._updateDisplay();
        }, 1000)
    }

    stop() {
        if (this._intervalId) clearInterval(this._intervalId)

    }
}

class PicrossEditor{

    // constructor(container) {
    //     this._container = container;

    // }

    static createTableContainer() {
    
        return [document.createElement("table"), document.createElement("tbody")];
    }

    createGrid(parent, rowNumber, columnNumber) {

        //Je fais une première boucle qui va correspondre à mes lignes et qui sera materialisé dans le DOM par des TR (= table row)
        for (let row = 1; row <= rowNumber; row++) {
            //a chaque tour de boucle :
            //je crée un tr
            let rowElement = document.createElement('tr');
    
            //dans ce tr, je vais faire une nouvelle boucle qui va correspondre aux colonnes, et qui sont matérialisées par des TD (= table data, qui sont forcément dans un tr)
            //CHAQUE TD EST UNE CASE
            for (let column = 1; column <= columnNumber; column++) {
    
                //je crée le td
                let columnElement = document.createElement('td');
    
                //Je lui ajoute le listener qui dit "quand je te clique dessus, tu ajoutes activated si tu l'as pas déjà, ou tu le supprimes si la classe existe"
                columnElement.addEventListener('click', toggleActivated);
    
                //j'accroche la case à la ligne
                rowElement.appendChild(columnElement);
            }
    
            //j'accroche la ligne à son parent (moi, ça sera tbody)
            parent.appendChild(rowElement);
        }
    
    }

    createColumnBooleanArray(tab, player = false) {
        const columnsArray = [];
        tab.forEach((line, lineNumber) => {
            if(player) {
                line.shift();
            }
            line.forEach((square, columnNumber) => {
                if (lineNumber === 0) {
                    columnsArray.push([]);
                }
                columnsArray[columnNumber][lineNumber] = square;
            })
        })
        return columnsArray;
    }
    
    createRowsBooleanArray(tab) {
        const booleanArray = [];
        // Pour chaque ligne :
        tab.forEach(row => {
           
            const squaresElements = row.querySelectorAll('td');
    
            let result = Array.from(squaresElements).map(square => square.classList.contains("activated"));

            booleanArray.push(result);
    
        });
        return booleanArray;
    }

    _getIndication(tab) {
        const indicationArray = [];
        let cellsActivatedNb = 0;
    
        tab.forEach(value => {
    
            if (value === false) {
                if (cellsActivatedNb > 0) {
                    indicationArray.push(cellsActivatedNb);
                    cellsActivatedNb = 0;
                }
            }
            else {
                cellsActivatedNb++;
            }
        })
    
        if (cellsActivatedNb > 0) {
            indicationArray.push(cellsActivatedNb);
    
        }
        return indicationArray;
    }
    getDatas(rowsBoolean, columnsBoolean) {
        console.log(rowsBoolean, columnsBoolean)
        return {
            datas: {
                rows: rowsBoolean.map(line => this._getIndication(line)),
                columns: columnsBoolean.map(column => this._getIndication(column))
            },
            size: {
                rows: rowsBoolean.length,
                columns: columnsBoolean.length
            }
        }
    }

}

class PicrossGame extends PicrossEditor {


}
const timer = new Timer(document.getElementById("timer"));

const editor = new PicrossEditor();



function toggleActivated(ev) {
    ev.currentTarget.classList.toggle('activated');
    const userPicross = getPicrossByPlayer();
    if (playedPicross) {

        if(JSON.stringify(playedPicross.datas) === JSON.stringify(userPicross.datas)) {
            timer.stop();
            document.getElementById("grid-container").appendChild(document.createElement("div")).textContent = "gagné"
    
        }
    }
}
function handleCreationSubmit(ev) {

    //permet d'eviter la soumission du formulaire

    ev.preventDefault();
    //si la table existe déjà, je la supprime
    emptyNode(document.querySelector("#grid-container"));


    //je récupére mon élement table et tbody qui sont créés avec la fonction createTableContainer(grâce au return)
    const [table, tbody] = PicrossEditor.createTableContainer(); //return [table, tbody];

    //j'accroche mon élement tbody à ma table
    table.appendChild(tbody);

    //j'accroche ma table à mon container
    document.getElementById('grid-container').appendChild(table);


    const form = document.getElementById('creation-form');

    //Je récupère mes valeurs du formulaire (grace à la propriété name="column" et name="row" dans le HTML
    const columnNumber = Number(form.column.value);
    const rowNumber = Number(form.row.value);

    //Je crée le tableau en lui précisant où il doit l'accrocher(tbody)
    editor.createGrid(tbody, rowNumber, columnNumber);

}

//creation de l'objet JSON
function handleExportSubmit(ev) {

    //permet d'eviter la soumission du formulaire
    ev.preventDefault();

    //je récupère mon formulaire
    const exportForm = document.getElementById('export-form');

    //je récupére mon tableau d'élements lignes
    const rowElements = document.querySelectorAll("tr");
    const rowsBoolean = editor.createRowsBooleanArray(rowElements);

    const columnsBoolean = editor.createColumnBooleanArray(rowsBoolean);
    

    //je crée l'objet qui va contenir les propriétés
    const picrossData = {
        title: exportForm.title.value,
        ...editor.getDatas(rowsBoolean, columnsBoolean)
    };


    emptyNode(document.querySelector("pre"));

    document.querySelector("pre").textContent = JSON.stringify(picrossData);
}


function createColumnBooleanArray(tab, player = false) {
    const columnsArray = [];
    tab.forEach((line, lineNumber) => {
        if(player) {
            line.shift();
        }
        line.forEach((square, columnNumber) => {
            if (lineNumber === 0) {
                columnsArray.push([]);
            }
            columnsArray[columnNumber][lineNumber] = square;
        })
    })
    return columnsArray;
}

function createRowsBooleanArray(tab) {
    const booleanArray = [];
    // Pour chaque ligne :
    tab.forEach(row => {
       
        const squaresElements = row.querySelectorAll('td');

        let result = Array.from(squaresElements).map(square => square.classList.contains("activated"));

        booleanArray.push(result);

    });
    return booleanArray;
}

async function getPicrossJsonAsync() {
    try {

        const response = await fetch("http://127.0.0.1:5500/data.json");

        const data = await response.json();

        const ul = document.body.appendChild(document.createElement("ul"));

        data.map(picross => {
            const li = document.createElement("li");
            li.textContent = picross.title;
            
            li.addEventListener("click", e => {

                timer.stop();
                const tableToDraw = createTableFromPicross(picross);

                emptyNode(document.querySelector("#grid-container"));

                document.querySelector("#grid-container").appendChild(tableToDraw);
                
                playedPicross = picross;
                timer.start();
            })
            ul.appendChild(li);

        })
        /* process your data further */
    } catch (error) {
        console.log(error)
    }

}
function displayVictory () {
    stop()
   
  }

  let intervalId;
  let currentTime;

  function resetTime () {
    currentTime = 0
    
  }
  function updateDisplay () {
    document.getElementById('timer').innerHTML = currentTime + ' s'
  }
  function start () {
    resetTime()
    intervalId = setInterval(() => {
      currentTime++
      updateDisplay()
    }, 1000)
  }
  function stop () {
    if (intervalId) clearInterval(intervalId)
  }

function createTableFromPicross(picross) {

    const { title, datas, size } = picross;
    
    const table = document.createElement("table");
    for (let line = 0; line < size.rows + 1; line++) {

        const lineElement = document.createElement("tr");

        for (let column = 0; column < size.columns + 1; column++) {

            const square = document.createElement("td");

            if (line === 0) {
                if (column > 0) {
                    square.innerHTML = datas.columns[column - 1].join('&nbsp;')

                }
            }
            if (column === 0) {
                if (line > 0) {
                    square.innerHTML = datas.rows[line - 1].join('&nbsp;')
                }
            }
            if (column > 0 && line > 0) {

                square.addEventListener('click', toggleActivated);
            }

            lineElement.appendChild(square);

        }

        table.appendChild(lineElement);
    }
    return table;
}




function getIndication(tab) {
    const indicationArray = [];
    let cellsActivatedNb = 0;

    tab.forEach(value => {

        if (value === false) {
            if (cellsActivatedNb > 0) {
                indicationArray.push(cellsActivatedNb);
                cellsActivatedNb = 0;
            }
        }
        else {
            cellsActivatedNb++;
        }
    })

    if (cellsActivatedNb > 0) {
        indicationArray.push(cellsActivatedNb);

    }
    return indicationArray;
}

function getPicrossByPlayer() {
    //je fais la matrice du picross obtenu à chaque click

    //AVec le spread, ca transforme en tableau et on vire le premier
    const [, ...rowElements] = document.querySelectorAll("tr");
    const rowsBoolean = createRowsBooleanArray(rowElements);
    const columnsBoolean = createColumnBooleanArray(rowsBoolean, true);

    return picrossData = {
        
        ...editor.getDatas(rowsBoolean, columnsBoolean)
    };
    
}


function emptyNode(node) {
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

document.getElementById("creation-submit").addEventListener('click', handleCreationSubmit);

document.getElementById("export-submit").addEventListener('click', handleExportSubmit);
getPicrossJsonAsync();
